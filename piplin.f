      program piplin
c
      integer i, j, n, pgbeg
      parameter ( n = 16384 )
      real fq1(n),fq2(n), v1(n),v2(n),v3(n)
      real t1r(n),t1l(n), t2r(n),t2l(n), t1i(n),t2i(n)
      real c1r(n),c1l(n), c2r(n),c2l(n), c1i(n),c2i(n)
      real bp1r(n),bp1l(n),bp2r(n),bp2l(n)
      real dotx(2), doty(2)
      character*5 temp, calchk
      character*10 obsdat
      character*12 rastr, decstr, label(5)
      character*15 keyword, keystr
      character*24 srcnam, calnam, callab, calday
      character*34 filout
      character*32 arg
      character*60 caldif
      character*80 line, junk
      logical twosat
      integer nchan, nchanc, bchan, echan, chans, bchanc, echanc,
     $ lstd, lsth, lstm, rah, ram, decd, decm, scann
      real vmin, vmax, ytext, xinc, smax, smin, mult,
     $ ch01r, ch01l, ch02r, ch02l, scal, eta1, eta2, bw,
     $ mjdc, dmjd, freq, lsts, ra10, dec10, ras, decs, el, elc
      double precision f1, f2, f3, f1sky, f2sky, f3sky, mjd, lstday
      external dopset
c
c
      write(6,*) 'Usage: pipeline smin smax vmin vmax mult'
      call getarg(1,arg)
      read(arg,'(F32.32)') smin
      call getarg(2,arg)
      read(arg,'(F32.32)') smax
      if ( smax .le. smin ) then
       smax = +101.0
       smin = -001.0
      end if
      call getarg(3,arg)
      read(arg,'(F32.32)') vmin
      call getarg(4,arg)
      read(arg,'(F32.32)') vmax
      call getarg(5,arg)
      read(arg,'(F32.32)') mult 
c
c open observation file and begin parsing
c header information to hand to dopset
      open(unit=10,status='old',file='target.ascii')
c 50 is not reached as long as the header is fewer than 50 lines
      do i = 1, 50
       read(10,'(A80)') line
       read(line,'(A15)') keyword
       if ( keyword .eq. '#     SRC_NAME=') then
        read(line,'(A15,A24)') keystr,srcnam
c        write(6,'(A15,A24)') keystr, srcnam
       else if ( keyword .eq. '#     DATE_OBS=' ) then
        read(line,'(A15,A10)') keystr,obsdat
       else if ( keyword .eq. '#      SCANNUM=' ) then
        read(line,'(A15,I6)') keystr,scann
       else if ( keyword .eq. '#          MJD=' ) then
        read(line,'(A15,F13.13)') keystr,mjd
        lstday = 6572.1572917D0 + 1.002737909350759D0 * mjd
        lstd = floor(real(lstday))
        lsth = floor((lstday-floor(lstday))*24.)
        lstm = floor(((lstday-real(lstd))*24.-real(lsth))*60.)
        lsts = (((lstday-real(lstd))*24.-real(lsth))*60.-real(lstm))*60.
       else if ( keyword .eq. '# Actual_FREQ2=' ) then
        read(line,'(A15,F13.13)') keystr,freq
        if ( freq .gt. 1650. ) then
         f1 = 1720.52998D+06
         f2 = 1667.35900D+06
         f3 = 1665.40184D+06
         twosat = .false.
        else if ( freq .lt. 1650. ) then
         f1 = 1720.52998D+06
         f2 = 1612.23100D+06
         f3 = 1612.23100D+06
         twosat = .true.
        else
              close(10)
              write(6,*) 'Check frequencies! Sky settings unknown!'
              go to 1000
        end if
       else if ( keyword .eq. '#        OBSBW=' ) then
        read(line,'(A15,F12.12)') keystr,bw
        bw = -1.*bw*1e+06
       else if ( keyword .eq. '#        NCHAN=' ) then
        read(line,'(A15,I5)') keystr,nchan
       else if ( keyword .eq. '#    STARTCHAN=' ) then
        read(line,'(A15,I5)') keystr,bchan
       else if ( keyword .eq. '#     STOPCHAN=' ) then
        read(line,'(A15,I5)') keystr,echan
c       else if ( keyword .eq. '#      RA(hrs)=' ) then
c        read (line,'(A15,F12.12)') keystr, ra10
c       else if ( keyword .eq. '#      RA(deg)=' ) then
       else if ( keyword .eq. '#         HREF=' ) then
        read (line,'(A15,F12.12)') keystr, ra10
        ra10 = ra10/15.
c       else if ( keyword .eq. '#     DEC(deg)=' ) then
       else if ( keyword .eq. '#         VREF=' ) then
        read (line,'(A15,F12.12)') keystr, dec10
        decd = floor(dec10)
        decm = floor((dec10-real(decd))*60.)
        decs = ((dec10-real(decd))*60.-real(decm))*60.
        write(decstr,'(I3.2,I2.2,I2.2,F0.4)')
     $   decd, decm, int(decs), decs-real(int(decs))
       else if ( keyword .eq. '#    ELEV(deg)=' ) then
        read (line,'(A15,F12.12)') keystr, el
       else if ( keyword .eq. '#Freq1(MHz)   ' ) then
        go to 10
       else
        continue
       end if
      end do
10    continue
       rah = floor(ra10)
       ram = floor((ra10-real(rah))*60.)
       ras = ((ra10-real(rah))*60.-real(ram))*60.
       write(rastr,'(I2.2,I2.2,I2.2,F0.5)')
     $  rah, ram, int(ras), ras-real(int(ras))
       nchans = echan - bchan + 1
c calibrator file
      open(unit=12,file='cal.ascii',status='old')
      do j = 1, 50
       read(12,'(A80)') line
       read(line,'(A15)') keyword
       if ( keyword .eq. '#     SRC_NAME=') then
        read(line,'(A15,A24)') keystr,calnam
        read(calnam,'(A5)') temp
c set fluxes to Ott values for 1700 MHz (or else VLA manual)
        if ( temp .eq. '3C147' ) then
         scal = 19.316
        else if ( temp .eq. '3C48-' ) then
         scal = 14.001
        else if ( temp .eq. '3C418' ) then
         scal =  5.8 * (1400./1700.)**0.7
        else if ( temp .eq. '3C394' ) then
         scal =  2.8 * (1400./1700.)**0.7
        else if ( temp .eq. '3C161' ) then
         scal = 16.206
        else if ( temp .eq. '3C353' ) then
         scal = 48.991
        else if ( temp .eq. '3C286' ) then
         scal = 13.395
        else if ( temp .eq. '3C295' ) then
         scal = 18.893
        else if ( temp .eq. '3C348' ) then
         scal = 38.629
        else if ( temp .eq. '3C123' ) then
         scal = 40.767
        else if ( temp .eq. '3C227' ) then
         scal =  6.411
        else if ( temp .eq. '3C380' ) then
         scal = 14.2 * (1400./1700.)**0.7
        else if ( temp .eq. 'BLLac' ) then
         scal =  6.1 * (1400./1700.)**0.7
        else if ( temp .eq. '3C84-' ) then
         scal = 23.9 * (1400./1700.)**0.7
        else if ( temp .eq. '3C119' ) then
         scal =  8.6 * (1400./1700.)**0.7
        else
              close(10)
              close(12)
              write(6,*) 'Calibrator Unknown!'
              go to 1000
        end if
        write(callab,'(A5,A1,F4.1,A2)') calnam,'-',scal,'Jy'
       else if ( keyword .eq. '#        NCHAN=' ) then
        read(line,'(A15,I5)') keystr,nchanc
        if ( nchan .ne. nchanc ) then
              close(10)
              close(12)
              write(6,*) 'Calibrator has different NChan!'
              go to 1000
        end if
       else if ( keyword .eq. '#    STARTCHAN=' ) then
        read(line,'(A15,I5)') keystr,bchanc
        if ( bchan .ne. bchanc ) then
              close(10)
              close(12)
              write(6,*) 'Calibrator has different BChan!'
              go to 1000
        end if
       else if ( keyword .eq. '#     STOPCHAN=' ) then
        read(line,'(A15,I5)') keystr,echanc
        if ( echan .ne. echanc ) then
              close(10)
              close(12)
              write(6,*) 'Calibrator has different EChan!'
              go to 1000
        end if
       else if ( keyword .eq. '#          MJD=' ) then
        read(line,'(A15,F13.13)') keystr,mjdc
        dmjd = abs(mjd-mjdc)
        write(calday,'(A8,F5.2,A11)') 'Cal obs ',dmjd,' days away '
       else if ( keyword .eq. '#    ELEV(deg)=' ) then
        read (line,'(A15,F12.12)') keystr, elc
       else if ( keyword .eq. '#Freq1(MHz)   ' ) then
        go to 20
       else
        continue
       end if
      end do
20    continue
      if ( i .ne. j) then
              close(10)
              close(12)
              write(6,*) 'Target and Cal headers different lengths!'
              go to 1000
      end if
c
c assemble dopset.inp for sky frequency call
      open(unit=11,status='unknown',file='dopset.inp')
      write(11,'(A6)') 'DUMMY'
      write(11,'(A4,F18.9,A1,F3.1,A1,I1,A1,I1,A1)')
     $  '''L'',',f1/1E+06,',',0.0,',',0,',',0,'/'
      write(11,'(A4,F18.9,A1,F3.1,A1,I1,A1,I1,A1)')
     $  '''B'',',f2/1E+06,',',0.0,',',0,',',0,'/'
      write(11,'(A4,F18.9,A1,F3.1,A1,I1,A1,I1,A1)')
     $  '''N'',',f3/1E+06,',',0.0,',',0,',',0,'/'
      write(11,'(A4,A12,A1,A12,A1,F6.1,A1)')
     $  '''C'',',rastr, ',',decstr,',',2000.,'/'
      write(11,'(A4,I5,A1,I2,A1,I2,A1,F12.8,A1)')
     $  '''D'',',lstd,',',lsth,',',lstm,',',lsts,'/'
      write(11,'(A4)') '''E''/'
      close(11)
c close file and execute dopset
      call dopset(f1sky,f2sky,f3sky)
c
c begin building ch0s here
      ch01r = 0.
      ch01l = 0.
      ch02r = 0.
      ch02l = 0.
c read in remainder of spectra files
      do i = 1, n
       if ( i .lt. nchans-1 ) then
c note on FQ numbers
c in the Skynet interface, 1669 is entered in the first blank but
c appears as Freq2 in the output spectra, so it is labelled IF2 here
        read(12,'(F14.14, F14.14, F14.14, F14.14, F14.14, F14.14)')
     $   fq1(i), c1l(i), c1r(i), fq2(i), c2l(i), c2r(i)
        if ( i .gt. nchans/16 .and. i .le. 15*nchans/16 ) then
         ch01r = ch01r + c1r(i)
         ch01l = ch01l + c1l(i)
         ch02r = ch02r + c2r(i)
         ch02l = ch02l + c2l(i)
        end if
        read(10,'(F14.14, F14.14, F14.14, F14.14, F14.14, F14.14)')
     $   fq1(i), t1l(i), t1r(i), fq2(i), t2l(i), t2r(i)
        v1(i) = (f1sky-fq1(i))/f1*1E+06*2.998E+05
        v2(i) = (f2sky-fq2(i))/f2*1E+06*2.998E+05
        v3(i) = (f3sky-fq2(i))/f3*1E+06*2.998E+05
       else
              t1r(i) = -1.
              t1l(i) = -1.
              t2r(i) = -1.
              t2l(i) = -1.
              t1i(i) = -1.
              t2i(i) = -1.
              c1r(i) = -1.
              c1l(i) = -1.
              c2r(i) = -1.
              c2l(i) = -1.
              c1i(i) = -1.
              c2i(i) = -1.
              bp1r(i) = -1.
              bp1l(i) = -1.
              bp2r(i) = -1.
              bp2l(i) = -1.
              fq1(i) = -1.
              fq2(i) = -1.
              v1(i) = -1.
              v2(i) = -1.
              v3(i) = -1.
       end if
      end do
      close(10)
      close(12)
      ch01r = ch01r/(7.*real(nchans)/8.)
      ch01l = ch01l/(7.*real(nchans)/8.)
      ch02r = ch02r/(7.*real(nchans)/8.)
      ch02l = ch02l/(7.*real(nchans)/8.)
      eta1 = (ch01r+ch01l)/2.*2.*1.38E-23
     $ *exp(0.009/sin(elc*3.14159/180.))/scal
      eta2 = (ch02r+ch02l)/2.*2.*1.38E-23
     $ *exp(0.009/sin(elc*3.14159/180.))/scal
      do i = 1, nchans
       bp1r(i) = c1r(i)/ch01r
       bp1l(i) = c1l(i)/ch01l
       bp2r(i) = c2r(i)/ch02r
       bp2l(i) = c2l(i)/ch02l
       t1r(i) = t1r(i)/bp1r(i)
       t1l(i) = t1l(i)/bp1l(i)
       t2r(i) = t2r(i)/bp2r(i)
       t2l(i) = t2l(i)/bp2l(i)
       c1r(i) = c1r(i)/bp1r(i)
       c1l(i) = c1l(i)/bp1l(i)
       c2r(i) = c2r(i)/bp2r(i)
       c2l(i) = c2l(i)/bp2l(i)
       t1i(i) = (t1r(i)+t1l(i))*mult
     $ /2.*2.*1.38E-23*exp(0.009/sin(el*3.14159/180.))/eta1
       t2i(i) = (t2r(i)+t2l(i))*mult
     $ /2.*2.*1.38E-23*exp(0.009/sin(el*3.14159/180.))/eta2
       c1i(i) = (c1r(i)+c1l(i))
     $ /2.*2.*1.38E-23*exp(0.009/sin(elc*3.14159/180.))/eta1
       c2i(i) = (c2r(i)+c2l(i))
     $ /2.*2.*1.38E-23*exp(0.009/sin(elc*3.14159/180.))/eta2
      end do
      if ( c1i(int(nchans/2)) .gt. scal*1.1
     $  .or. c1i(int(nchans/2)) .lt. scal*0.9
     $  .or. c2i(int(nchans/2)) .gt. scal*1.1
     $  .or. c2i(int(nchans/2)) .lt. scal*0.9 ) then
              write(6,*) 'Calibration not internally consistent!'
              go to 1000
      end if
c apply computed eta from band 1 to band 2 and vice versa as a way to
c check for RFI and other problems in CH0; display this on BP page
      write(caldif,'(A36,F5.1,A19)')
     $ 'Measured calibrator fluxes differ by',
     $ (abs((c1r(int(nchans/2))+c1l(int(nchans/2)))/2.
     $ *2.*1.38E-23*exp(0.009/sin(elc*3.14159/180.))/eta2-scal)
     $ + abs((c2r(int(nchans/2))+c2l(int(nchans/2)))/2.
     $ *2.*1.38E-23*exp(0.009/sin(elc*3.14159/180.))/eta1-scal)
     $ )/2./scal*100.
     $ ,'% in the two bands.'
c
c output files
      write(filout,'(A7,A1,A10,A1,I5,A1,I4,A4)')
     $ srcnam,'_',obsdat,'-',scann,'_',int(f2/1E+06),'.dat'
      open(unit=14,file=filout,status='unknown')
      do i = 3, nchans-3
       write(14,'(F15.5,F10.3)') v2(i), t2i(i)
      end do
      write(filout,'(A7,A1,A10,A1,I5,A1,I4,A4)')
     $ srcnam,'_',obsdat,'-',scann,'_',int(f3/1E+06),'.dat'
      open(unit=15,file=filout,status='unknown')
      do i = 3, nchans-3
       write(15,'(F15.5,F10.3)') v3(i), t2i(i)
      end do
      write(filout,'(A7,A1,A10,A1,I5,A1,I4,A4)')
     $ srcnam,'_',obsdat,'-',scann,'_',int(f1/1E+06),'.dat'
      open(unit=16,file=filout,status='unknown')
      do i = 3, nchans-3
       write(16,'(F15.5,F10.3)') v1(i), t1i(i)
      end do
      close(14)
      close(15)
      close(16)
c
c --== plotting ==--
c
c labels and fiducial lines
      dotx(1) = -500.
      dotx(2) = 1800.
      ytext = smax+(smax-smin)*0.1
      xinc = (vmin-vmax)*0.200
      write(label(1),'(A12)') srcnam
      write(label(2),'(I12)') int(f3/1E+06)
      write(label(3),'(I12)') int(f1/1E+06)
      write(label(4),'(I12)') int(f2/1E+06)
      write(label(5),'(A12)') callab
c pgplot preamble
c      if ( pgbeg(0,'/XWINDOW',1,1) .ne. 1 ) stop
      if ( pgbeg(0,'out.ps/CPS',1,1) .ne. 1 ) stop
c      if ( pgbeg(0,'out.ps/VCPS',1,1) .ne. 1 ) stop
c      call pgask ( .false. )
      call pgscr(8, 0.00, 0.50, 0.00)
      call pgscr(9, 0.50, 0.50, 0.50)
      call pgslw(3)
c first page --- target spectra
c bottom half
      call pgsvp(  0.15, 0.95, 0.15, 0.525)
      call pgswin( vmin  , vmax  , smin   , smax    )
c
      call pgsci(8)
      call pgsls(1)
      call pgbin(nchans-3, v1, t1i, .true.)
      call pgptxt(vmin-xinc*(1-1),smax-(smax-smin)*0.2,0.,0.,label(3))
      call pgsls(1)
      call pgsci(1)
      call pgsch(1.0)
      call pgptxt(vmin-xinc*(1-1),smax-(smax-smin)*0.1,0.0,-1.0,'IF1')
c make axes
      call pgsch(1.25)
      call pgslw(3)
      call pgsci(1)
      call pgbox ( 'bcnts', 0.0, 0, 'bcnts', 0.0, 0 )
      call pglab (
     $              'v\dLSR\u (km s\u-1\d)',
     $              'S\d\gn\u (Jy)',
     $              '')
c top half
      call pgslw(3)
      call pgsci(1)
      call pgsvp(  0.15, 0.95, 0.525, 0.90 )
      call pgswin( vmin ,  vmax  , smin   , smax    )
c
      call pgsch(1.00)
      call pgsci(1)
      call pgptxt(vmin-xinc*(1-1),ytext,0.0,0.0,label(1))
      call pgsci(5)
      call pgptxt(vmin-xinc*(2-1),ytext,0.0,0.0,label(5))
      call pgptxt(vmin-xinc*(3-1),ytext,0.0,0.0,calday)
      call pgsci(1)
      call pgptxt(vmin-xinc*(5-1),ytext,0.0,0.0,obsdat)
      call pgptxt(vmin-xinc*(1-1),smax-(smax-smin)*0.1,0.0,-1.0,'IF2')
c
      call pgsls(1)
      call pgsci(4)
      call pgbin(nchans-3, v2, t2i, .true.)
      call pgptxt(vmin-xinc*(1-1),smax-(smax-smin)*0.2,0.,0.,label(4))
      if ( .not. twosat ) then
       call pgsci(2)
       call pgbin(nchans-3, v3, t2i, .true.)
       call pgptxt(vmin-xinc*(1-1),smax-(smax-smin)*0.3,0.,0.,label(2))
      end if
c make axes
      call pgsch(1.25)
      call pgsls(1)
      call pgslw(3)
      call pgsci(1)
      call pgbox ( 'bcts', 0.0, 0, 'bcmts', 0.0, 0 )
      call pglab (
     $              '',
     $              'S\d\gn\u (Jy)',
     $              '')
c
      call pgsch(1.00)
      call pgsls(1)
      call pgsci(1)
      call pgpage(.false.)
c
c next page --- bandpass tables
c bottom half
      call pgslw(3)
      call pgsvp(  0.15, 0.95, 0.15, 0.500)
      call pgswin( fq1(nchans-5)  , fq1(5)  , +000.00, +002.10 )
c
      call pgsls(1)
      call pgsci(2)
      call pgsch(1.00)
      call pgbin(nchans-3, fq1, bp1r, .true.)
      call pgptxt(fq1(nchans-5),1.80,0.0,-0.3,'IF1 Ypol')
      call pgsci(4)
      call pgbin(nchans-3, fq1, bp1l, .true.)
      call pgptxt(fq1(nchans-5),1.60,0.0,-0.3,'IF1 Xpol')
      doty(1) = 1.
      doty(2) = 1.
      call pgsls(2)
      call pgsci(9)
      call pgline(2,dotx,doty)
c make axes
      call pgsci(1)
      call pgsls(1)
      call pgslw(3)
      call pgsch(1.25)
      call pgbox ( 'bcnts', 0.0, 0, 'bcnts', 0.0, 0 )
      call pglab (
     $              'sky frequency (MHz)',
     $              'bandpass factor',
     $              '')
c
      call pgslw(3)
      call pgsvp(  0.15, 0.95, 0.500, 0.90 )
      call pgswin( fq2(nchans-5)  , fq2(5)  , +000.00, +002.10 )
c
      call pgsch(1.00)
      call pgsls(1)
      call pgsci(3)
      call pgbin(nchans-3, fq2, bp2r, .true.)
      call pgptxt(fq2(nchans-5),1.80,0.0,-0.3,'IF2 Ypol')
      call pgsci(5)
      call pgbin(nchans-3, fq2, bp2l, .true.)
      call pgptxt(fq2(nchans-5),1.60,0.0,-0.3,'IF2 Xpol')
      doty(1) = 1.
      doty(2) = 1.
      call pgsls(2)
      call pgsci(9)
      call pgline(2,dotx,doty)
c make axes
      call pgsci(1)
      call pgsls(1)
      call pgslw(3)
      call pgsch(1.25)
      call pgbox ( 'bcmts', 0.0, 0, 'bcmts', 0.0, 0 )
      call pglab (
     $              '',
     $              'bandpass factor',
     $              caldif)
c
      call pgsch(1.00)
      call pgsls(1)
      call pgsci(1)
      call pgend
c
1000  continue
      stop
      end
c
