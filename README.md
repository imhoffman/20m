Automated reduction for [the GBO 20-m](http://www.gb.nrao.edu/20m/) given target and cal on/off inputs from the butterfly link ([example](http://www.gb.nrao.edu/20m/peak/W75N-028/Skynet_57642_W75N-028_23819_23874.A.html)).

The program `pipeline` looks to the symbolic links `target.ascii` and `cal.ascii` for the scans downloaded from [the 20m server](http://www.gb.nrao.edu/20m/peak/log.htm) and generates a postscript plot and writes out calibrated spectra as ascii files.

This program uses an altered version of the DOPSET code of the NRAO.
